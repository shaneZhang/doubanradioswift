//
//  PlayButton.swift
//  DoubanRadioSwift
//
//  Created by shaneZhang on 15/5/3.
//  Copyright (c) 2015年 shaneZhang. All rights reserved.
//

import UIKit

class PlayButton: UIButton
{

    var isPlay:Bool = true
    let imgPlay:UIImage = UIImage(named: "play")!
    let imgPause:UIImage = UIImage(named: "pause")!
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        self.addTarget(self, action: "onClick", forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    func onClick()
    {
        isPlay = !isPlay
        if isPlay
        {
            self.setImage(imgPause, forState: UIControlState.Normal)
        }
        else
        {
            self.setImage(imgPlay, forState: UIControlState.Normal)
        }
    }
    
    func onPlay()
    {
        isPlay = true
        self.setImage(imgPause, forState: UIControlState.Normal)
    }

}
