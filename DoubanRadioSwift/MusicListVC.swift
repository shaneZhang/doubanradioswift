//
//  MusicListVC.swift
//  DoubanRadioSwift
//
//  Created by shaneZhang on 15/5/3.
//  Copyright (c) 2015年 shaneZhang. All rights reserved.
//

import UIKit

protocol MusicListVCDelegate
{
    func onChangeChannel(channel_Id:NSString)
}




class MusicListVC: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!

    var delegate:MusicListVCDelegate?
    
    var channelData:[JSON] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return   channelData.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ChannelMusicListCell") as! UITableViewCell
        
        let rowData:JSON = channelData[indexPath.row] as JSON
        
        cell.textLabel?.text = rowData["name"].string
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let rowData:JSON = self.channelData[indexPath.row] as JSON
        let channel_id = rowData["channel_id"].stringValue
        
        delegate?.onChangeChannel(channel_id)
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}
