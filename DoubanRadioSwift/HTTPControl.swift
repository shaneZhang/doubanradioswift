//
//  HTTPControl.swift
//  DoubanRadioSwift
//
//  Created by shaneZhang on 15/5/3.
//  Copyright (c) 2015年 shaneZhang. All rights reserved.
//

import UIKit

class HTTPController:NSObject
{
    //定义一个代理
    var delegate:HttpControlDelegate?
    //接收网址，回调代理的方法传回数据
    func onSearch(url:String)
    {
        Alamofire.manager.request(Method.GET, url).responseJSON(options: NSJSONReadingOptions.MutableContainers)
            { (_, _, data, error) -> Void in
            self.delegate?.didRecieveNetworkingResults(data!)
            }
    }
}

//定义http协议
protocol HttpControlDelegate
{
    //定义一个方法，接收一个参数：AnyObject
    func didRecieveNetworkingResults(results:AnyObject)
}
