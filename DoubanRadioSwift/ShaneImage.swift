//
//  ShaneImage.swift
//  DoubanRadioSwift
//
//  Created by shaneZhang on 15/5/3.
//  Copyright (c) 2015年 shaneZhang. All rights reserved.
//

import UIKit

class ShaneImage: UIImageView
{
   
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        
        self.clipsToBounds = true
        
        self.layer.cornerRadius = self.frame.size.width * 0.5
        
        self.layer.borderColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7).CGColor
        
        self.layer.borderWidth = 4
        
    }
    
    
    func onRotation()
    {
        var animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.fromValue = 0.0
        animation.toValue = M_PI * 2.0
        animation.duration = 20
        animation.repeatCount = 10000
        self.layer.addAnimation(animation, forKey: nil)
    }
    
}
